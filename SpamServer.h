#ifndef SPAMMALWARESERVER_SPAMSERVER_H
#define SPAMMALWARESERVER_SPAMSERVER_H

#include <string>
#include "Network.h"
#include "Content.h"

class SpamServer
{
public:
    bool MakeSpamRequest() const;
    SpamServer() = delete;
    explicit SpamServer(const std::string& AdminPanelUrl,const std::string& ZipFilePath);
    ~SpamServer() = default;

    SpamServer(const SpamServer&) = delete;
    SpamServer(SpamServer&&) = delete;
    SpamServer& operator=(const SpamServer&) = delete;
    SpamServer& operator=(SpamServer&&) = delete;

private:
    Network requestWorker;
    Content contentGenerator;
};

#endif //SPAMMALWARESERVER_SPAMSERVER_H
