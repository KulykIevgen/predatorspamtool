cmake_minimum_required(VERSION 3.10)
project(SpamMalwareServer)

set(CMAKE_CXX_STANDARD 17)

set(HEADERS SpamServer.h Network.h Content.h)
set(SOURCES main.cpp SpamServer.cpp Network.cpp Content.cpp)

add_executable(${PROJECT_NAME} ${HEADERS} ${SOURCES})
target_link_libraries(${PROJECT_NAME} Wininet)