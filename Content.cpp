#include "Content.h"
#include <fstream>
#include <algorithm>

Content::Content(const std::string &PathToArchieve):ZipArchievePath(PathToArchieve)
{

}

std::vector<uint8_t> Content::ProduceData() const
{
    std::vector<uint8_t> zipContent;
    std::ifstream zipFile(ZipArchievePath,std::ios::binary);

    std::for_each(std::istreambuf_iterator<char>(zipFile),std::istreambuf_iterator<char>(),
                  [&zipContent](char symbol) -> void
    {
       zipContent.push_back(static_cast<uint8_t>(symbol));
    });

    return zipContent;
}
