#ifndef SPAMMALWARESERVER_CONTENT_H
#define SPAMMALWARESERVER_CONTENT_H

#include <string>
#include <vector>
#include <cstdint>

class Content
{
public:
    explicit Content(const std::string& PathToArchieve);
    Content() = delete;
    std::vector<uint8_t> ProduceData() const;
    ~Content() = default;

    Content(const Content&) = delete;
    Content(Content&&) = delete;
    Content& operator=(const Content&) = delete;
    Content& operator=(Content&&) = delete;

private:
    std::string ZipArchievePath;
};

#endif //SPAMMALWARESERVER_CONTENT_H