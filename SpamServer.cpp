#include "SpamServer.h"

SpamServer::SpamServer(const std::string &AdminPanelUrl,const std::string& ZipFilePath):
        requestWorker(AdminPanelUrl),
        contentGenerator(ZipFilePath)
{

}

bool SpamServer::MakeSpamRequest() const
{
    const auto spamContent = contentGenerator.ProduceData();
    return requestWorker.SendData(spamContent);
}
