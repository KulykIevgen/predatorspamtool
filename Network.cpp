#include "Network.h"
#include <algorithm>

Network::Network(const std::string &AdminPanel)
{
    inet = InternetOpenA(UserAgent.c_str(),INTERNET_OPEN_TYPE_PRECONFIG,nullptr,nullptr,0);
    if (!inet)
    {
        throw std::logic_error("Bad InternetOpen call");
    }
    connection = InternetConnectA(inet,AdminPanel.c_str(),80, nullptr,nullptr,INTERNET_SERVICE_HTTP,0,1);
    if (!connection)
    {
        InternetCloseHandle(inet);
        throw std::logic_error("Bad InternetConnect call");
    }
}

Network::~Network() noexcept
{
    InternetCloseHandle(inet);
    InternetCloseHandle(connection);
}

bool Network::SendData(const std::vector<uint8_t> &data) const
{
    bool result = false;
    auto handle = HttpOpenRequestA(connection,RequestType.c_str(),RequestValue.c_str(),nullptr,nullptr,nullptr,
                     INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_KEEP_CONNECTION |
                     INTERNET_FLAG_PRAGMA_NOCACHE,1);
    if (handle)
    {
        std::vector<uint8_t> fullData;
        std::copy(Prolog.cbegin(),Prolog.cend(),std::back_inserter(fullData));
        std::copy(data.cbegin(),data.cend(),std::back_inserter(fullData));

        result = static_cast<bool>( HttpSendRequestA(handle,Headers.c_str(),Headers.length(),
                                                    const_cast<uint8_t*> ( fullData.data() ),fullData.size()) );
        InternetCloseHandle(handle);
    }

    return result;
}
